This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for gCube Software Versions Processor Lib

## [v1.1.0-SNAPSHOT]

- Renamed some classes to improve terminology
- Added sphinx documentation [#24841]

## [v1.0.0]

- First Release

