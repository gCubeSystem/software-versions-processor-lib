package org.gcube.common.software.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class Config {

	protected static Properties properties;
	protected static final String PROPERTIES_FILENAME = "config.properties";
	
	static {
		properties = new Properties();
		InputStream input = Config.class.getClassLoader().getResourceAsStream(PROPERTIES_FILENAME);
		
		try {
			// load the properties file
			properties.load(input);
		} catch(IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static Properties getProperties() {
		return properties;
	}
	
}
