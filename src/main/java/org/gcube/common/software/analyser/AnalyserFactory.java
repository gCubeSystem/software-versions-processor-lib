package org.gcube.common.software.analyser;

import java.io.File;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.common.software.utils.Utils;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class AnalyserFactory {
	
	public static final String EXPORT_FILENAME_EXTENSION = ".json";
	
	public static final String CONFIGURATION_PROPERTY_NAME = "configuration";
	public static final String ARTIFACTS_PROPERTY_NAME = "artifacts";
	
	public static Analyser getAnalyser(File jsonFile) throws Exception {
		ObjectMapper objectMapper = Utils.getObjectMapper();
		JsonNode jsonNode = objectMapper.readTree(jsonFile);
		return getAnalyser(jsonNode);
	}
	
	public static Analyser getAnalyser(String inputJson) throws Exception {
		ObjectMapper objectMapper = Utils.getObjectMapper();
		JsonNode inputNode = objectMapper.readTree(inputJson);
		return getAnalyser(inputNode);
	}
	
	public static Analyser getAnalyser(JsonNode inputNode) throws Exception {
		Analyser analyser = new Analyser();
		ObjectNode originalGlobalConfiguration = (ObjectNode) inputNode.get(CONFIGURATION_PROPERTY_NAME);
		analyser.setGlobalConfiguration(originalGlobalConfiguration);
		ArrayNode originalArtifactMetadataArray = (ArrayNode) inputNode.get(ARTIFACTS_PROPERTY_NAME);
		analyser.setArtifactMetadataArray(originalArtifactMetadataArray);
		return analyser;
	}
	
	public static Analyser getAnalyser(String localConfiguration, String inputJson) throws Exception {
		ObjectMapper objectMapper = Utils.getObjectMapper();
		JsonNode localConfigurationNode = objectMapper.readTree(localConfiguration);
		JsonNode inputNode = objectMapper.readTree(inputJson);
		return getAnalyser(localConfigurationNode, inputNode);
	}
	
	public static Analyser getAnalyser(JsonNode localConfiguration, JsonNode inputNode) throws Exception {
		Analyser analyser = new Analyser();
		ObjectNode inputConfiguration = (ObjectNode) inputNode.get(CONFIGURATION_PROPERTY_NAME);
		ObjectNode mergedConfiguration = (ObjectNode) Utils.merge(localConfiguration, inputConfiguration);
		analyser.setGlobalConfiguration(mergedConfiguration);
		ArrayNode originalArtifactMetadataArray = (ArrayNode) inputNode.get(ARTIFACTS_PROPERTY_NAME);
		analyser.setArtifactMetadataArray(originalArtifactMetadataArray);
		return analyser;
	}
	
}
