package org.gcube.common.software.utils;

import java.io.File;
import java.net.URL;

import org.gcube.common.software.processor.biblatex.BibLaTeXExporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class FileUtils {

	private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);
	
	public static File getFileFromFilename(String fileName) throws Exception {
		URL jsonFileURL = BibLaTeXExporter.class.getClassLoader().getResource(fileName);
		File file = new File(jsonFileURL.toURI());
		logger.trace("File is {}", file.getAbsolutePath());
		return file;
	}
	
}
