package org.gcube.common.software.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.SerializationFeature;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class Utils {
	
	/**
	 * DateTime Pattern to be used to serialize Dates
	 */
	public static final String DATETIME_PATTERN = "yyyy-MM-dd";

	public static String getDateAsString(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATETIME_PATTERN);
		return simpleDateFormat.format(date);
	}
	
	protected static ObjectMapper objectMapper;
	
	static {
		objectMapper = new ObjectMapper();
		SimpleDateFormat sdf = new SimpleDateFormat(Utils.DATETIME_PATTERN);
		objectMapper.setDateFormat(sdf);
		objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
	}
	
	public static ObjectMapper getObjectMapper() {
		return objectMapper;
	}
	
	public static String replaceVariable(String variableName, String replace, String s) {
		return s.replaceAll("\\{\\{" + variableName + "\\}\\}", replace);
	}
	
	/**
	 * Clone node1 and merge all the properties contained in node2 into the cloned JsonNode.
	 * In other words, the properties contained in node2 
	 * will replace/add the properties contained in the clone of node1.
	 * Both node1 and node2 are not modified.
	 * @param node1 contains the properties to be merged with the properties contained in node2.
	 * @param node2 contains the properties will replace/add the properties in the clone of node1.
	 * @return a new JsonNode containing the merged properties.
	 */
	public static JsonNode merge(JsonNode node1, JsonNode node2) {
		ObjectNode cloned = node1.deepCopy();
		Iterator<String> iterator = node2.fieldNames();
		while (iterator.hasNext()) {
			String fieldName = iterator.next();
			JsonNode value = node2.get(fieldName);
			cloned.replace(fieldName, value.deepCopy());
		}
		return cloned;
	}
}
