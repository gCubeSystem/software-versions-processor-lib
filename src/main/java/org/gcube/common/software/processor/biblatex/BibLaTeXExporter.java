package org.gcube.common.software.processor.biblatex;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Set;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.common.software.model.ElaborationType;
import org.gcube.common.software.model.Variables;
import org.gcube.common.software.processor.SoftwareArtifactProcessor;
import org.gcube.common.software.utils.FileUtils;
import org.gcube.common.software.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class BibLaTeXExporter extends SoftwareArtifactProcessor {

	private static final Logger logger = LoggerFactory.getLogger(BibLaTeXExporter.class);

	public static final String EXPORT_FILENAME_EXTENSION = ".bib";
	public static final String TEMPLATE_FILENAME = "biblatex.template";

	public BibLaTeXExporter() {
		super(BibLaTeXExporter.EXPORT_FILENAME_EXTENSION);
	}
	
	@Override
	public void export() throws Exception {
		if(first) {
			File exportFile = getOutputFile();
			if (exportFile.exists()) {
				exportFile.delete();
			}
			exportFile.createNewFile();
		}
		
		String title = softwareArtifactMetadata.getTitle();

		ElaborationType export = processorConfig.getElaborationType();
		
		switch (export) {
			case ALL:
				generate();
				break;
	
			case UPDATE_ONLY:
				if (softwareArtifactMetadata.isNewDeposition()) {
					logger.info("Skipping export for {}.", title);
					return;
				}
				generate();
				break;
	
			case NEW:
				if (!softwareArtifactMetadata.isNewDeposition()) {
					logger.info("Skipping export for {}.", title);
					return;
				}
				generate();
				break;
	
			case NONE:
			default:
				logger.info("Skipping export for {}.", title);
				return;
		}

	}

	protected String getTemplate() throws Exception {
		File file = FileUtils.getFileFromFilename(TEMPLATE_FILENAME);
		byte[] bytes = Files.readAllBytes(file.toPath());
		String template = new String(bytes, StandardCharsets.UTF_8);
		return template;
	}

//	private String getCitationID() {
//		String name = softwareVersion.getName();
//		StringBuffer stringBuffer = new StringBuffer();
//		stringBuffer.append(name);
//		stringBuffer.append("_");
//		stringBuffer.append(softwareVersion.getVersion());
//		return stringBuffer.toString();
//	}

	private String getAuthors(ArrayNode arrayNode) {
		StringBuffer stringBuffer = new StringBuffer();
		int size = arrayNode.size();
		for (int i = 0; i < size; i++) {
			JsonNode jsonNode = arrayNode.get(i);
			stringBuffer.append("{");
			// Surname, Name
			String fullName = jsonNode.get("name").asText();

			// Surname: nameParts[0]
			// Name: nameParts[1]
			String[] nameParts = fullName.split(",");
			stringBuffer.append(nameParts[1].trim());
			stringBuffer.append(" ");
			stringBuffer.append(nameParts[0].trim());

			stringBuffer.append("}");
			if (i < (size - 1)) {
				stringBuffer.append(" and ");
			}
		}
		return stringBuffer.toString();
	}
	
	private String getKeywords(Set<String> keywords) {
		StringBuffer stringBuffer = new StringBuffer();
		int size = keywords.size();
		String[] array = keywords.toArray(new String[size]);
		for (int i = 0; i < size; i++) {
			stringBuffer.append(array[i]);
			if (i < (size - 1)) {
				stringBuffer.append(", ");
			}
		}
		return stringBuffer.toString();
	}

//	private String addNotes(String s) {
//		StringBuffer note = new StringBuffer();
//		note.append("\n\tnote = {");
//		boolean noteFound = false; 
//		String gCubeReleaseVersion = softwareVersion.getGCubeReleaseVersion(); 
//		if(gCubeReleaseVersion!=null) {
//			noteFound = true;
//			note.append("gCube Release Version: ");
//			note.append(softwareVersion.getGCubeReleaseVersion());
//			note.append("\n");
//		}
//		URL gCubeReleaseTicket = softwareVersion.getGCubeReleaseTicket();
//		if(gCubeReleaseTicket != null) {
//			noteFound = true;
//			note.append("gCube Release Ticket: ");
//			note.append(gCubeReleaseTicket.toString());
//			note.append("\n");
//		}
//		note.append("\t},");
//		
//		if(noteFound) {
//			note.insert(0, s.substring(0,s.length()-2));
//			note.append("\n}");
//			s = note.toString();
//		}
//		return s;
//	}
	
	protected String parseTemplate(String template) throws Exception {
		String s = template;
		s = Utils.replaceVariable("author", getAuthors(softwareArtifactMetadata.getAuthors()), s);
		s = Utils.replaceVariable("keywords", getKeywords(softwareArtifactMetadata.getKeywords()), s);
		
		Variables variables = softwareArtifactMetadata.getVariables();
		s = variables.replaceAllVariables(s);
//		s = addNotes(s);
		
		return s;
	}

	protected void generate() throws Exception {
		String title = softwareArtifactMetadata.getTitle();
		if(softwareArtifactMetadata.getVersionDOIURL()==null) {
			logger.info("No Version DOI URL for {}. It will not be exported in BibLaTex format.", title);
			return;
		}
		logger.info("Going to export {} in BibLaTex format.", title);

		String template = getTemplate();
		String output = parseTemplate(template);
		
		writeToFile(output);
	}
	
	protected void writeToFile(String output) throws Exception {
		File exportFile = getOutputFile();
		FileWriter fileWritter = new FileWriter(exportFile, true);
		BufferedWriter bw = new BufferedWriter(fileWritter);
		bw.write(output);
		bw.write("\n\n");
		bw.close();
	}

}
