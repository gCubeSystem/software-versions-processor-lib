package org.gcube.common.software.processor;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.gcube.common.software.model.ProcessorConfig;
import org.gcube.common.software.model.GlobalConfig;
import org.gcube.common.software.model.SoftwareArtifactMetadata;
import org.gcube.common.software.processor.biblatex.BibLaTeXExporter;
import org.gcube.common.software.processor.zenodo.ZenodoExporter;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class SoftwareArtifactProcessor {

	protected static Map<String, Class<? extends SoftwareArtifactProcessor>> availableProcessors;
	
	static {
		availableProcessors = new HashMap<>();
		add(ZenodoExporter.class);
		add(BibLaTeXExporter.class);
	}
	
	private static void add(Class<? extends SoftwareArtifactProcessor> clz) {
		availableProcessors.put(clz.getSimpleName(), clz);
	}
	
	public static Map<String, Class<? extends SoftwareArtifactProcessor>> getAvailableProcessors() {
		return availableProcessors;
	}
	
	protected File outputDirectory;
	protected GlobalConfig globalConfig;
	protected SoftwareArtifactMetadata softwareArtifactMetadata;
	protected ProcessorConfig processorConfig;
	
	protected boolean first;
	protected boolean last;
	
	protected final String exportFileNameExtension; 
	
	protected SoftwareArtifactProcessor(String exportFileNameExtension) {
		this.exportFileNameExtension = exportFileNameExtension;
	}
	
	public void setOutputDirectory(File outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

	public GlobalConfig getGlobalConfig() {
		return globalConfig;
	}

	public void setGlobalConfig(GlobalConfig globalConfig) {
		this.globalConfig = globalConfig;
	}

	public SoftwareArtifactMetadata getSoftwareArtifactConfig() {
		return softwareArtifactMetadata;
	}

	public void setSoftwareArtifactConfig(SoftwareArtifactMetadata softwareArtifactMetadata) {
		this.softwareArtifactMetadata = softwareArtifactMetadata;
	}
	
	public ProcessorConfig getProcessorConfig() {
		return processorConfig;
	}
	
	public void setProcessorConfig(ProcessorConfig processorConfig) {
		this.processorConfig = processorConfig;
	}

	public void setFirst(boolean first) {
		this.first = first;
	}
	
	public void setLast(boolean last) {
		this.last = last;
	}

	public abstract void export() throws Exception;
	
	public File getOutputFile() throws Exception {
		String fileName = globalConfig.getFileName() + exportFileNameExtension;
		File file = new File(outputDirectory, fileName);
		return file;
	}
	
}
