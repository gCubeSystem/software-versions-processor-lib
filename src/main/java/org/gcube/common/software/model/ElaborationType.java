package org.gcube.common.software.model;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public enum ElaborationType {

	ALL,
	UPDATE_ONLY,
	NEW,
	NONE
	
}
