package org.gcube.common.software.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import org.gcube.com.fasterxml.jackson.annotation.JsonAnyGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonAnySetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.node.JsonNodeType;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GlobalConfig {
	
	public static final String EXPORT_FILENAME_PROPERTY_NAME = "export_filename";
	
	public static final String PROCESSORS_PROPERTY_NAME = "processors";

	@JsonProperty(PROCESSORS_PROPERTY_NAME)
	protected Map<String,ProcessorConfig> processorConfigurations;
	
	protected Map<String, JsonNode> properties;
	
	@JsonIgnore
	protected ObjectNode originalJson;
	
	public GlobalConfig() {
		this.properties = new LinkedHashMap<>(); 
	}

	public Map<String,ProcessorConfig> getProcessorConfigurations() {
		return processorConfigurations;
	}
	
	@JsonIgnore
	public ObjectNode getOriginalJson() {
		return originalJson;
	}

	@JsonIgnore
	public void setOriginalJson(ObjectNode originalJson) {
		this.originalJson = originalJson;
	}
	
	@JsonAnyGetter
    public Map<String, JsonNode> getProperties() {
		return properties;
    }
	
	@JsonAnySetter
    public void addProperty(String key, JsonNode value) {
		this.properties.put(key, value);
    }
	
	@JsonIgnore
	public JsonNode getProperty(String key) {
		return this.properties.get(key);
	}
	
	protected String getExportFileName(String fileName) {
		if(fileName==null || fileName.length()==0) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
			return simpleDateFormat.format(Calendar.getInstance().getTime());
		}
		return fileName;
	}
	
	public String getFileName() {
		JsonNode jsonNode = getProperty(EXPORT_FILENAME_PROPERTY_NAME);
		String fileName = jsonNode.getNodeType() == JsonNodeType.NULL ? null : jsonNode.asText();
		return fileName = getExportFileName(fileName);
	}

}
