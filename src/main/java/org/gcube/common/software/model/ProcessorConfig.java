package org.gcube.common.software.model;

import java.util.LinkedHashMap;
import java.util.Map;

import org.gcube.com.fasterxml.jackson.annotation.JsonAnyGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonAnySetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonFormat;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ProcessorConfig {

	public static final String ELABORATION_PROPERTY_NAME = "elaboration";
	
	@JsonProperty(ELABORATION_PROPERTY_NAME)
	@JsonFormat(shape=JsonFormat.Shape.STRING)
	protected ElaborationType elaboration;

	protected Map<String, JsonNode> properties;
	
	public ProcessorConfig() {
		properties = new LinkedHashMap<>();
	}
	
	public ElaborationType getElaborationType() {
		return elaboration;
	}
	
	@JsonAnyGetter
    public Map<String, JsonNode> getProperties() {
		return properties;
    }
	
	@JsonAnySetter
    public void addProperty(String key, JsonNode value) {
		this.properties.put(key, value);
    }
	
	@JsonIgnore
	public JsonNode getProperty(String key) {
		return this.properties.get(key);
	}
	
}
