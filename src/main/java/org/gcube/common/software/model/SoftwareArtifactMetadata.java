package org.gcube.common.software.model;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.gcube.com.fasterxml.jackson.annotation.JsonAnyGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonAnySetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonFormat;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.common.software.utils.Utils;

/**
 * @author Luca Frosini (ISTI - CNR)
 * TO BE GENERALIZED
 */
public class SoftwareArtifactMetadata {

	public static final String PREVIOUS_CONCEPT_DOI_VALUE = "PREVIOUS";
	
	public static final String NAME_PROPERTY_NAME = "name";
	public static final String VERSION_PROPERTY_NAME = "version";
	public static final String TITLE_PROPERTY_NAME = "title";
	public static final String DATE_PROPERTY_NAME = "date";
	public static final String LICENSE_PROPERTY_NAME = "license";
	public static final String KEYWORDS_PROPERTY_NAME = "keywords";
	public static final String DESCRIPTION_PROPERTY_NAME = "description";
	public static final String AUTHORS_PROPERTY_NAME = "authors";
	public static final String FILES_PROPERTY_NAME = "files";
	public static final String CODE_LOCATION_PROPERTY_NAME = "code_location";
	public static final String CONCEPT_DOI_URL_PROPERTY_NAME = "concept_doi_url";
	public static final String VERSION_DOI_URL_PROPERTY_NAME = "version_doi_url";
	public static final String GRANTS_PROPERTY_NAME = "grants";
	
	@JsonIgnore
	protected SoftwareArtifactMetadata previous;

	@JsonIgnore
	protected SoftwareArtifactMetadata next;
	
	@JsonIgnore
	protected Boolean newDeposition;
	
	@JsonIgnore
	protected ObjectNode originalJson;
	
	@JsonProperty(NAME_PROPERTY_NAME)
	protected String name;
	
	@JsonProperty(VERSION_PROPERTY_NAME)
	protected String version;

	@JsonProperty(DATE_PROPERTY_NAME)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Utils.DATETIME_PATTERN)
	protected Date date;
	
	@JsonProperty(TITLE_PROPERTY_NAME)
	protected String title;
	
	@JsonProperty(LICENSE_PROPERTY_NAME)
	protected JsonNode license;
	
	@JsonProperty(KEYWORDS_PROPERTY_NAME)
	protected Set<String> keywords;
	
	@JsonProperty(AUTHORS_PROPERTY_NAME)
	protected ArrayNode authors;
	
	@JsonProperty(FILES_PROPERTY_NAME)
	protected List<SoftwareArtifactFile> files;
	
	@JsonProperty(CODE_LOCATION_PROPERTY_NAME)	
	protected String codeLocation;
	
	@JsonProperty(CONCEPT_DOI_URL_PROPERTY_NAME)	
	protected String conceptDOIURL;
	
	@JsonProperty(VERSION_DOI_URL_PROPERTY_NAME)
	protected String versionDOIURL;
	
	@JsonProperty(GRANTS_PROPERTY_NAME)
	protected JsonNode grants;
	

	
	protected Map<String, JsonNode> additionalProperties;
	
	public SoftwareArtifactMetadata() {
		this.newDeposition = false;
		this.additionalProperties = new LinkedHashMap<>(); 
	}

	@JsonIgnore
	public SoftwareArtifactMetadata getPrevious() {
		return previous;
	}

	@JsonIgnore
	public void setPrevious(SoftwareArtifactMetadata previous) {
		this.previous = previous;
		if(previous!=null) {
			this.previous.next = this;
		}
	}

	@JsonIgnore
	public SoftwareArtifactMetadata getNext() {
		return next;
	}
	
	@JsonIgnore
	public Boolean isNewDeposition() {
		return newDeposition;
	}

	@JsonIgnore
	public void setNewDeposition(Boolean newVersion) {
		this.newDeposition = newVersion;
	}
	
	@JsonIgnore
	public Variables getVariables() throws Exception {
		ObjectMapper objectMapper = Utils.getObjectMapper();
		JsonNode jsonNode = objectMapper.valueToTree(this);
		Variables variables = objectMapper.treeToValue(jsonNode, Variables.class);
		Set<String> missingVariables = variables.parse();
		int size = missingVariables.size();
		if(size>0) {
			throw new Exception("The following variables has been used but not defined or cannot be actualised" + 
					missingVariables.toArray(new String[size]).toString());
		}
		return variables;
	}
	
	@JsonIgnore
	public ObjectNode getOriginalJson() {
		return originalJson;
	}

	@JsonIgnore
	public void setOriginalJson(ObjectNode originalJson) {
		this.originalJson = originalJson;
	}
	
	public String getName() {
		return name;
	}
	
	public String getVersion() {
		return version;
	}

	public Date getDate() {
		return date;
	}
	
	public String getTitle() {
		return title;
	}

	public JsonNode getLicense() {
		return license;
	}

	public Set<String> getKeywords() {
		return keywords;
	}

	public ArrayNode getAuthors() {
		return authors;
	}
	
	public List<SoftwareArtifactFile> getFiles() {
		return files;
	}

	public String getCodeLocation() {
		return codeLocation;
	}

	public String getConceptDOIURL() {
		if(conceptDOIURL!=null && conceptDOIURL.compareTo(PREVIOUS_CONCEPT_DOI_VALUE)==0) {
			if(previous!=null) {
				conceptDOIURL = previous.getConceptDOIURL();
			}else {
				throw new RuntimeException(CONCEPT_DOI_URL_PROPERTY_NAME + " is indicates as " + PREVIOUS_CONCEPT_DOI_VALUE + " but " + getTitle() + " has no previous. Please check you config.");
			}
		}
		return conceptDOIURL;
	}

	public void setConceptDOIURL(String conceptDOIURL) {
		this.conceptDOIURL = conceptDOIURL;
	}

	public String getVersionDOIURL() {
		return versionDOIURL;
	}
	
	public void setVersionDOIURL(String versionDOIURL) {
		this.versionDOIURL = versionDOIURL;
	}
	
	public JsonNode getGrants() {
		return grants;
	}

	@JsonAnyGetter
    public Map<String, JsonNode> getAdditionalProperties() {
		return additionalProperties;
    }
	
	@JsonAnySetter
    public void addAdditionalProperty(String key, JsonNode value) {
		this.additionalProperties.put(key, value);
    }
	
	@JsonIgnore
	public JsonNode getAdditionalProperty(String key) {
		return this.additionalProperties.get(key);
	}
	
}