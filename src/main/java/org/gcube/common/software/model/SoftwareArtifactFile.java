package org.gcube.common.software.model;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;
import org.gcube.com.fasterxml.jackson.annotation.JsonSetter;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SoftwareArtifactFile {

	@JsonProperty(value="url")
	protected URL url;
	
	@JsonProperty(value="desired_name")
	protected String desiredName;
	
	
	public URL getURL() {
		return url;
	}
	
	public void setURL(URL url) {
		this.url = url;
	}
	
	@JsonSetter(value="url")
	public void setURL(String url) throws MalformedURLException {
		this.url = new URL(url);
	}
	
	public String getDesiredName() {
		return desiredName;
	}
	
	public void setDesiredName(String desiredName) {
		this.desiredName = desiredName;
	}
	
	public File downloadFile() throws IOException {
		File file = new File(desiredName);
		
		Path path = Paths.get(desiredName);
		try (InputStream inputStream = url.openStream()) {
			Files.copy(inputStream, path, StandardCopyOption.REPLACE_EXISTING);
		}
        
        /* Uses apache common-io */
        // FileUtils.copyURLToFile(df.getURL(), file);
        
        if(!file.exists()) {
        	throw new RuntimeException(file.getAbsolutePath() + " does not exist");
        }
        if(file.length()==0) {
        	throw new RuntimeException(file.getAbsolutePath() + " has size 0");
        }
		return file;
	}
}
