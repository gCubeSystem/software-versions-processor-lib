package org.gcube.common.software.model;

import java.io.File;
import java.util.Set;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.common.software.analyser.AnalyserTest;
import org.gcube.common.software.utils.FileUtils;
import org.gcube.common.software.utils.Utils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class VariablesTest {

	private static final Logger logger = LoggerFactory.getLogger(VariablesTest.class);
	
	@Test
	public void testFindVariables() {
		Variables variables = new Variables();
		Set<String> vars = variables.findVariables("https://nexus.d4science.org/nexus/service/local/repo_groups/gcube-releases-all/content/org/gcube/data-catalogue/{{name}}/{{version}}/{{name}}-{{version}}.war");
		logger.info("Found variables are {}", vars);
	}
	
	@Test
	public void test() throws Exception {
		File file = FileUtils.getFileFromFilename(AnalyserTest.FILENAME);
		ObjectMapper objectMapper = Utils.getObjectMapper();
		JsonNode jsonNode = objectMapper.readTree(file);
		JsonNode concept = jsonNode.get("concept");
		
		logger.info("{}", objectMapper.writeValueAsString(concept));
		
		Variables variables = objectMapper.treeToValue(concept, Variables.class);
		
//		logger.info("Variables are {}\n\n", variables.getProperties());
		
		Set<String> missingVariables = variables.parse();
		logger.info("Missing variables are {}", missingVariables);
		
//		logger.info("Variables after parsing are {}\\n\\n", variables.getProperties());
		
		
		JsonNode swConcept = objectMapper.convertValue(variables.getProperties(), JsonNode.class);
		logger.info("{}", objectMapper.writeValueAsString(swConcept));
		
	}
}
