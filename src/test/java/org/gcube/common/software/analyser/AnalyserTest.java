package org.gcube.common.software.analyser;

import java.io.File;
import java.util.List;

import org.gcube.common.software.utils.FileUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class AnalyserTest {

	private static final Logger logger = LoggerFactory.getLogger(Analyser.class);
	
	// public static final String FILENAME = "gcat-test-sandbox.json";
	// public static final String FILENAME = "gcat-from-scratch.json";
	public static final String FILENAME = "gcat-doc.json";
	
	@Test
	public void testUsingTestFile() throws Exception {
		File file = FileUtils.getFileFromFilename(FILENAME);
		Analyser analyser = AnalyserFactory.getAnalyser(file);
		// analyser.setOutputDirectory(file.getParentFile());
		List<File> files = analyser.analyse();
		logger.info("Generated the following files {}", files);
	}

}
