{
    "configuration": {
    	"processors": {
	        "ZenodoExporter": {
	        	"elaboration": "NONE",
	            "skip_grants": ["004260"],
	            "additional_html_description": "\n\n<p><a href=\"https://www.gcube-system.org/\">gCube</a> is an open-source software toolkit used for building and operating Hybrid Data Infrastructures enabling the dynamic deployment of Virtual Research Environments, such as the <a href=\"https://www.d4science.org/\">D4Science Infrastructure</a>, by favouring the realisation of reuse-oriented policies.</p>\n\n<p><a href=\"https://www.gcube-system.org/\">gCube</a> has been used to successfully build and operate infrastructures and virtual research environments for application domains ranging from biodiversity to environmental data management and cultural heritage.</p>\n\n<p><a href=\"https://www.gcube-system.org/\">gCube</a> offers components supporting typical data management workflows including data access, curation, processing, and visualisation on a large set of data typologies ranging from primary biodiversity data to geospatial and tabular data.</p>\n\n<p><a href=\"https://www.d4science.org/\">D4Science</a> is a Hybrid Data Infrastructure combining over 500 software components and integrating data from more than 50 different data providers into a coherent and managed system of hardware, software, and data resources. The D4Science infrastructure drastically reduces the cost of ownership, maintenance, and operation thanks to the exploitation of gCube.</p>\n\n<p>&nbsp;</p>\n\n<p>The official source code location of this software version is available at:</p>\n\n<p><a href=\"{{code_location}}\">{{code_location}}</a></p>"
	        },
	        "BibLaTeXExporter": {
	        	 "elaboration": "ALL"
	        }
	    },
        "name": "gcat",
        "group": "data-catalogue",
        "title": "gCube Catalogue (gCat) Service {{version}}",
        "license": {
            "id": "EUPL-1.1",
            "url": "https://opensource.org/licenses/EUPL-1.1"
        },
        "keywords": ["gCube", "Catalogue", "D4Science"],
        "description": "gCube Catalogue (gCat) Service allows the publication of items in the gCube Catalogue.",
		"html_description": "<p>{{description}}</p>",
        "authors": [
            {
                "affiliation": "Istituto di Scienza e Tecnologie dell'Informazione \"A. Faedo\" - CNR, Italy",
                "name": "Frosini, Luca",
                "orcid": "0000-0003-3183-2291"
            }
        ],
        "files": [
            {
                "url": "https://code-repo.d4science.org/gCubeSystem/{{name}}/archive/v{{version}}.zip",
                "desired_name": "{{name}}-v{{version}}.zip"
            },
            {
                "url": "https://code-repo.d4science.org/gCubeSystem/{{name}}/archive/v{{version}}.tar.gz",
                "desired_name": "{{name}}-v{{version}}.tar.gz"
            },
            {
                "url": "https://nexus.d4science.org/nexus/service/local/repo_groups/gcube-releases-all/content/org/gcube/{{group}}/{{name}}/{{version}}/{{name}}-{{version}}.war",
                "desired_name": "{{name}}-v{{version}}.war"
            }
        ],
        "concept_doi_url": "https://doi.org/10.5072/zenodo.1139068",
        "code_location": "https://code-repo.d4science.org/gCubeSystem/{{name}}/releases/tag/v{{version}}",
        "communities": [
            {
                "identifier": "gcube-system"
            }
        ],
        "grants": [
            {
                "id": "004260",
                "name": "DILIGENT",
                "url": "https://cordis.europa.eu/project/id/004260"
            },
            {
                "id": "654119",
                "name": "PARTHENOS",
                "url": "https://cordis.europa.eu/project/id/654119"
            },
			{
                "id": "675680",
                "name": "BlueBRIDGE",
                "url": "https://cordis.europa.eu/project/id/675680"
            }
        ],
		"export_filename": "{{name}}"
    },
    "artifacts": [
        {
            "version": "1.0.0",
            "date": "2019-01-10",
            "group": "data-publishing",
            "files": [
                {
                    "url": "https://nexus.d4science.org/nexus/service/local/repositories/gcube-snapshots/content/org/gcube/data-publishing/gcat/1.0.0-SNAPSHOT/gcat-1.0.0-20190109.172827-2.war",
                    "desired_name": "{{name}}-v{{version}}.war"
                }
            ],
            "gcube_release_version": null,
            "gcube_release_ticket": null,
            "concept_doi_url": "https://doi.org/10.5072/zenodo.1139445",
            "version_doi_url": "https://doi.org/10.5072/zenodo.1139446",
            "code_location": "https://code-repo.d4science.org/gCubeSystem/{{name}}"
        },
        {
            "version": "1.1.0",
            "date": "2019-02-26",
            "group": "data-publishing",
            "files": [
                {
                    "url": "https://nexus.d4science.org/nexus/service/local/repo_groups/gcube-releases-all/content/org/gcube/data-publishing/gcat/1.1.0-4.13.1-177071/gcat-1.1.0-4.13.1-177071-src.zip",
                    "desired_name": "{{name}}-v{{version}}.zip"
                },
                {
                    "url": "https://nexus.d4science.org/nexus/service/local/repo_groups/gcube-releases-all/content/org/gcube/data-publishing/gcat/1.1.0-4.13.1-177071/gcat-1.1.0-4.13.1-177071.war",
                    "desired_name": "{{name}}-v{{version}}.war"
                }
            ],
            "gcube_release_version": "4.13.1",
            "gcube_release_ticket": "https://support.d4science.org/issues/12988",
            "concept_doi_url": "https://doi.org/10.5072/zenodo.1139445",
            "version_doi_url": "https://doi.org/10.5072/zenodo.1140461",
            "code_location": "https://code-repo.d4science.org/gCubeSystem/{{name}}"
        },
        {
            "version": "1.2.0",
            "date": "2019-05-20",
            "group": "data-publishing",
            "files": [
                {
                    "url": "https://nexus.d4science.org/nexus/service/local/repositories/gcube-snapshots/content/org/gcube/data-publishing/gcat/1.2.0-SNAPSHOT/gcat-1.2.0-20190520.132914-10.war",
                    "desired_name": "{{name}}-v{{version}}.war"
                }
            ],
            "gcube_release_version": null,
            "gcube_release_ticket": null,
            "concept_doi_url": "https://doi.org/10.5072/zenodo.1139445",
            "version_doi_url": "https://doi.org/10.5072/zenodo.1140750",
            "code_location": "https://code-repo.d4science.org/gCubeSystem/{{name}}"
        },
        {
            "version": "2.0.0",
            "date": "2021-05-04",
            "gcube_release_version": "5.2.0",
            "gcube_release_ticket": "https://support.d4science.org/issues/19738",
            "version_doi_url": "https://doi.org/10.5072/zenodo.1139069"
        },
		{
            "version": "2.1.0",
            "date": "2022-01-27",
            "gcube_release_version": "5.7.0",
            "gcube_release_ticket": "https://support.d4science.org/issues/21685/",
            "version_doi_url": "https://doi.org/10.5072/zenodo.1139070"
        }
    ]
}