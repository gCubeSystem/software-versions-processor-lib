{
    "configuration": {
        "name": "gcat",
        "group": "data-catalogue",
        "title": "gCube Catalogue (gCat) Service {{version}}",
        "license": {
            "id": "EUPL-1.1",
            "url": "https://opensource.org/licenses/EUPL-1.1"
        },
        "keywords": ["gCube", "Catalogue", "D4Science"],
        "description": "gCube Catalogue (gCat) Service allows the publication of items in the gCube Catalogue.",
		"html_description": "<p>{{description}}</p>",
        "authors": [
            {
                "affiliation": "Istituto di Scienza e Tecnologie dell'Informazione \"A. Faedo\" - CNR, Italy",
                "name": "Frosini, Luca",
                "orcid": "0000-0003-3183-2291"
            }
        ],
        "files": [
            {
                "url": "https://code-repo.d4science.org/gCubeSystem/{{name}}/archive/v{{version}}.zip",
                "desired_name": "{{name}}-v{{version}}.zip"
            },
            {
                "url": "https://code-repo.d4science.org/gCubeSystem/{{name}}/archive/v{{version}}.tar.gz",
                "desired_name": "{{name}}-v{{version}}.tar.gz"
            },
            {
                "url": "https://nexus.d4science.org/nexus/service/local/repo_groups/gcube-releases-all/content/org/gcube/{{group}}/{{name}}/{{version}}/{{name}}-{{version}}.war",
                "desired_name": "{{name}}-v{{version}}.war"
            }
        ],
        "code_location": "https://code-repo.d4science.org/gCubeSystem/{{name}}/releases/tag/v{{version}}",
        "communities": [
            {
                "identifier": "gcube-system"
            }
        ],
        "grants": [
            {
                "id": "004260",
                "name": "DILIGENT",
                "url": "https://cordis.europa.eu/project/id/004260"
            },
            {
                "id": "212488",
                "name": "D4Science",
                "url": "https://cordis.europa.eu/project/id/212488"
            },
            {
                "id": "239019",
                "name": "D4Science-II",
                "url": "https://cordis.europa.eu/project/id/239019"
            },
            {
                "id": "283465",
                "name": "ENVRI",
                "url": "https://cordis.europa.eu/project/id/283465"
            },
            {
                "id": "283644",
                "name": "iMarine",
                "url": "https://cordis.europa.eu/project/id/283644"
            },
            {
                "id": "288754",
                "name": "EUBrazilOpenBio",
                "url": "https://cordis.europa.eu/project/id/288754"
            },
            {
                "id": "654024",
                "name": "SoBigData",
                "url": "https://cordis.europa.eu/project/id/654024"
            },
            {
                "id": "654119",
                "name": "PARTHENOS",
                "url": "https://cordis.europa.eu/project/id/654119"
            },
            {
                "id": "654142",
                "name": "EGI-Engage",
                "url": "https://cordis.europa.eu/project/id/654142"
            },
            {
                "id": "654182",
                "name": "ENVRI PLUS",
                "url": "https://cordis.europa.eu/project/id/654182"
            },
            {
                "id": "675680",
                "name": "BlueBRIDGE",
                "url": "https://cordis.europa.eu/project/id/675680"
            },
            {
                "id": "727610",
                "name": "PerformFISH",
                "url": "https://cordis.europa.eu/project/id/727610"
            },
            {
                "id": "731001",
                "name": "AGINFRA PLUS",
                "url": "https://cordis.europa.eu/project/id/731001"
            },
            {
                "id": "818194",
                "name": "DESIRA",
                "url": "https://cordis.europa.eu/project/id/818194"
            },
            {
                "id": "823914",
                "name": "ARIADNEplus",
                "url": "https://cordis.europa.eu/project/id/823914"
            },
            {
                "id": "824091",
                "name": "RISIS 2",
                "url": "https://cordis.europa.eu/project/id/824091"
            },
            {
                "id": "857650",
                "name": "EOSC-Pillar",
                "url": "https://cordis.europa.eu/project/id/857650"
            },
            {
                "id": "862409",
                "name": "Blue Cloud",
                "url": "https://cordis.europa.eu/project/id/862409"
            },
            {
                "id": "871042",
                "name": "SoBigData-PlusPlus",
                "url": "https://cordis.europa.eu/project/id/871042"
            }
        ],
		"export_filename": "{{name}}",
	    "processors": {
	        "ZenodoExporter": {
	        	"elaboration": "NONE",
	            "skip_grants": ["004260"],
	            "additional_html_description": "\n\n<p><a href=\"https://www.gcube-system.org/\">gCube</a> is an open-source software toolkit used for building and operating Hybrid Data Infrastructures enabling the dynamic deployment of Virtual Research Environments, such as the <a href=\"https://www.d4science.org/\">D4Science Infrastructure</a>, by favouring the realisation of reuse-oriented policies.</p>\n\n<p><a href=\"https://www.gcube-system.org/\">gCube</a> has been used to successfully build and operate infrastructures and virtual research environments for application domains ranging from biodiversity to environmental data management and cultural heritage.</p>\n\n<p><a href=\"https://www.gcube-system.org/\">gCube</a> offers components supporting typical data management workflows including data access, curation, processing, and visualisation on a large set of data typologies ranging from primary biodiversity data to geospatial and tabular data.</p>\n\n<p><a href=\"https://www.d4science.org/\">D4Science</a> is a Hybrid Data Infrastructure combining over 500 software components and integrating data from more than 50 different data providers into a coherent and managed system of hardware, software, and data resources. The D4Science infrastructure drastically reduces the cost of ownership, maintenance, and operation thanks to the exploitation of gCube.</p>\n\n<p>&nbsp;</p>\n\n<p>The official source code location of this software version is available at:</p>\n\n<p><a href=\"{{code_location}}\">{{code_location}}</a></p>"
	        },
	        "BibLaTeXExporter": {
	        	 "elaboration": "ALL"
	        }
	    }
    },
    "artifacts": [
        {
            "version": "1.0.0",
            "date": "2019-01-10",
            "group": "data-publishing",
            "files": [
                {
                    "url": "https://nexus.d4science.org/nexus/service/local/repositories/gcube-snapshots/content/org/gcube/data-publishing/gcat/1.0.0-SNAPSHOT/gcat-1.0.0-20190109.172827-2.war",
                    "desired_name": "{{name}}-v{{version}}.war"
                }
            ],
            "gcube_release_version": null,
            "gcube_release_ticket": null,
            "concept_doi_url": null,
            "version_doi_url": null,
            "code_location": "https://code-repo.d4science.org/gCubeSystem/{{name}}"
        },
        {
            "version": "1.1.0",
            "date": "2019-02-26",
            "group": "data-publishing",
            "files": [
                {
                    "url": "https://nexus.d4science.org/nexus/service/local/repo_groups/gcube-releases-all/content/org/gcube/data-publishing/gcat/1.1.0-4.13.1-177071/gcat-1.1.0-4.13.1-177071-src.zip",
                    "desired_name": "{{name}}-v{{version}}.zip"
                },
                {
                    "url": "https://nexus.d4science.org/nexus/service/local/repo_groups/gcube-releases-all/content/org/gcube/data-publishing/gcat/1.1.0-4.13.1-177071/gcat-1.1.0-4.13.1-177071.war",
                    "desired_name": "{{name}}-v{{version}}.war"
                }
            ],
            "gcube_release_version": "4.13.1",
            "gcube_release_ticket": "https://support.d4science.org/issues/12988",
            "concept_doi_url": "PREVIOUS",
            "version_doi_url": null,
            "code_location": "https://code-repo.d4science.org/gCubeSystem/{{name}}"
        },
        {
            "version": "1.2.0",
            "date": "2019-05-20",
            "group": "data-publishing",
            "files": [
                {
                    "url": "https://nexus.d4science.org/nexus/service/local/repositories/gcube-snapshots/content/org/gcube/data-publishing/gcat/1.2.0-SNAPSHOT/gcat-1.2.0-20190520.132914-10.war",
                    "desired_name": "{{name}}-v{{version}}.war"
                }
            ],
            "gcube_release_version": null,
            "gcube_release_ticket": null,
            "concept_doi_url": "PREVIOUS",
            "version_doi_url": null,
            "code_location": "https://code-repo.d4science.org/gCubeSystem/{{name}}"
        },
        {
            "version": "1.3.0",
            "date": "2019-06-27",
            "group": "data-publishing",
            "files": [
                {
                    "url": "https://nexus.d4science.org/nexus/service/local/repo_groups/gcube-releases-all/content/org/gcube/data-publishing/gcat/1.3.0-4.14.0-179505/gcat-1.3.0-4.14.0-179505-src.zip",
                    "desired_name": "{{name}}-v{{version}}.zip"
                },
                {
                    "url": "https://nexus.d4science.org/nexus/service/local/repo_groups/gcube-releases-all/content/org/gcube/data-publishing/gcat/1.3.0-4.14.0-179505/gcat-1.3.0-4.14.0-179505.war",
                    "desired_name": "{{name}}-v{{version}}.war"
                }
            ],
            "gcube_release_version": "4.14.0",
            "gcube_release_ticket": "https://support.d4science.org/issues/16743",
            "concept_doi_url": "PREVIOUS",
            "version_doi_url": null,
            "code_location": "https://code-repo.d4science.org/gCubeSystem/{{name}}"
        },
        {
            "version": "1.4.0",
            "date": "2019-11-20",
            "group": "data-publishing",
            "gcube_release_version": "4.15.0",
            "gcube_release_ticket": "https://support.d4science.org/issues/17294",
            "concept_doi_url": "PREVIOUS",
            "version_doi_url": null
        },
        {
            "version": "1.4.1",
            "date": "2019-12-20",
            "group": "data-publishing",
            "gcube_release_version": "4.18.0",
            "gcube_release_ticket": "https://support.d4science.org/issues/18335",
            "concept_doi_url": "PREVIOUS",
            "version_doi_url": null
        },
        {
            "version": "1.4.2",
            "date": "2020-02-14",
            "group": "data-publishing",
            "gcube_release_version": "4.20.0",
            "gcube_release_ticket": "https://support.d4science.org/issues/18507",
            "concept_doi_url": "PREVIOUS",
            "version_doi_url": null            
        },
        {
            "version": "1.4.3",
            "date": "2020-06-16",
            "group": "data-publishing",
            "gcube_release_version": "4.23.0",
            "gcube_release_ticket": "https://support.d4science.org/issues/19322",
            "concept_doi_url": "PREVIOUS",
            "version_doi_url": null
        },
        {
            "version": "1.4.4",
            "date": "2021-02-24",
            "group": "data-publishing",
            "gcube_release_version": "5.0.0",
            "gcube_release_ticket": "https://support.d4science.org/issues/20648",
            "concept_doi_url": "PREVIOUS",
            "version_doi_url": null
        },
        {
            "version": "1.4.5",
            "date": "2021-03-31",
            "group": "data-publishing",
            "gcube_release_version": "5.1.0",
            "gcube_release_ticket": "https://support.d4science.org/issues/20920",
            "concept_doi_url": "PREVIOUS",
            "version_doi_url": null
        },
        {
            "version": "2.0.0",
            "date": "2021-05-04",
            "gcube_release_version": "5.2.0",
            "concept_doi_url": null,
            "version_doi_url": null
        },
		{
            "version": "2.1.0",
            "date": "2022-01-27",
            "gcube_release_version": "5.7.0",
            "gcube_release_ticket": "https://support.d4science.org/issues/21685/",
            "concept_doi_url": "PREVIOUS",
            "version_doi_url": null
        },
		{
            "version": "2.2.0",
            "date": "2022-05-12",
            "gcube_release_version": "5.11.0",
            "gcube_release_ticket": "https://support.d4science.org/issues/22943",
            "concept_doi_url": "PREVIOUS",
            "version_doi_url": null
        },
		{
            "version": "2.3.0",
            "date": "2022-07-22",
            "gcube_release_version": "5.13.0",
            "gcube_release_ticket": "https://support.d4science.org/issues/23374",
            "concept_doi_url": "PREVIOUS",
            "version_doi_url": null
        },
		{
            "version": "2.4.0",
            "date": "2022-09-16",
            "gcube_release_version": "5.13.1",
            "gcube_release_ticket": "https://support.d4science.org/issues/23650",
            "concept_doi_url": "PREVIOUS",
            "version_doi_url": null
        },
		{
            "version": "2.4.1",
            "date": "2022-12-07",
            "gcube_release_version": "5.14.0",
            "gcube_release_ticket": "https://support.d4science.org/issues/23885",
            "concept_doi_url": "PREVIOUS",
            "version_doi_url": null
        }
    ]
}